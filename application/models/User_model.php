<?php
class User_model extends CI_Model {
    public function data($id = false, $level = false){
        if($id){
            $this->db->where('id_user', $id);
        }
        $this->db->join('level','user.id_level = level.id_level');
        if($level !== "Administrator"){
            if($level === "Waiter"){
                $arr = array("Pelanggan", "Waiter");
                $this->db->or_where_in('nama_level', $arr);
                $this->db->order_by('nama_level','DESC');                
            }else{  
                $this->db->where('nama_level', $level);
            }
        }
        return $this->db->get('user');
    }
    public function level(){
        return $this->db->get('level');
    }
    public function tambah($data){
        return $this->db->insert('user', $data);
    }
    public function update($id, $data){
        $this->db->where('id_user', $id);
        return $this->db->update('user', $data);
    }
    public function hapus($id){
        $this->db->where('id_user', $id);
        return $this->db->delete('user');
    }
}
?>