<?php
class Masakan_model extends CI_Model {
    public function data($id = false, $status = false){
        if($id){
            $this->db->where('id_masakan', $id);
        }
        if($status){
            $this->db->where('status_masakan','Tersedia');
        }
        return $this->db->get('masakan');
    }
    public function hapus($id){
        $this->db->where('id_masakan', $id);
        return $this->db->delete('masakan');
    }
    public function tambah($data){
        // $this->db->where('id_masakan', $id);
        return $this->db->insert('masakan', $data);
    }
    public function update($id, $data){
        $this->db->where('id_masakan', $id);
        return $this->db->update('masakan', $data);
    }
}
?>