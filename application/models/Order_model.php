<?php
class Order_model extends CI_Model {
    public function data($id = false){
        if($id){
            $this->db->where('id_order', $id);
        }
        $this->db->join('user','pesan.id_user = user.id_user');
        $this->db->join('level','user.id_level = level.id_level');
        $this->db->join('meja','pesan.id_meja = meja.id_meja');
        $this->db->order_by('id_order','DESC');
        return $this->db->get('pesan');
    }
    public function insert($data){
        return $this->db->insert('pesan', $data);
    }
    public function detail($tambah){
        return $this->db->insert('detail_order', $tambah);
    }
    public function detail_order($id){
        $this->db->join('pesan', 'detail_order.id_order = pesan.id_order');
        $this->db->join('meja', 'pesan.id_meja = meja.id_meja');
        $this->db->join('masakan', 'detail_order.id_masakan = masakan.id_masakan');
        $this->db->join('user', 'pesan.id_user = user.id_user');
        $this->db->where('detail_order.id_order', $id);
        return $this->db->get('detail_order');
    }
    public function hapus($id){
        $this->db->where('id_masakan', $id);
        return $this->db->delete('masakan');
    }
    public function update_status($id, $status){
        $this->db->where('id_order', $id);
        return $this->db->update('pesan', $status);
    }
    public function update_meja($id, $no_meja){
        $this->db->where('id_order', $id);
        return $this->db->update('meja', $status);
    }
}
?>