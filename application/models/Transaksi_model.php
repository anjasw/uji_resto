<?php
class Transaksi_model extends CI_Model {
    public function data($id = false){
        if($id){
            $this->db->where('id_order', $id);
        }
        $this->db->join('user','pesan.id_user = user.id_user');
        $this->db->join('level','user.id_level = level.id_level');
        $this->db->join('meja','pesan.id_meja = meja.id_meja');
        $this->db->where('status_order', 'Proses');
        return $this->db->get('pesan');
    }
    public function bayar($data){
        return $this->db->insert('transaksi', $data);
    }
    public function data_log(){
        $this->db->join('user', 'log_transaksi.id_user = user.id_user');
        $this->db->join('level', 'user.id_level = level.id_level');
        $this->db->join('pesan', 'log_transaksi.id_order = pesan.id_order');
        $this->db->join('meja', 'pesan.id_meja = meja.id_meja');
        return $this->db->get('log_transaksi');
    }
}