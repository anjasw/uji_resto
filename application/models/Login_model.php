<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    public function check_login($username, $password){
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->join('level', 'user.id_level = level.id_level');
        return $this->db->get('user')->row();
    }
}