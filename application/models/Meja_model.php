<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meja_model extends CI_Model {
    public function status($no_meja, $data){
        $this->db->where('no_meja', $no_meja);
        return $this->db->update('meja',$data);
    }
    public function nomor($id){
        $this->db->where('id_order', $id);
        $this->db->limit(1);
        return $this->db->get('pesan')->row();
    }
    public function data($id = false){
        if($id){
            $this->db->where('id_meja', $id);
        }
        return $this->db->get('meja');
    }
}