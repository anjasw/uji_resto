<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{

    public function __construct(){
        parent::__construct();
		$this->load->model('Login_model','lm');
    }
    
    public function index(){
        $data['msg'] = '';
        if($this->input->method(TRUE) == "POST"){
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$cek = $this->lm->check_login($username, $password);
			if($cek){
				$session['username'] = $cek->username;
				$session['nama'] = $cek->nama_user;
				$session['level'] = $cek->nama_level;
				$session['id_level'] = $cek->id_level;
				$session['id_user'] = $cek->id_user;
				$this->session->set_userdata("sudah_login", $session);
				if($cek->nama_level === "Administrator" || $cek->nama_level === "Waiter" || $cek->nama_level === "Owner" || $cek->nama_level === "Kasir"){
					redirect('home');
				}elseif($cek->nama_level === "Pelanggan"){
					redirect('pesanan/tambah');										
				}
			}else{
				$data['msg'] = 'Materialize.toast("Username dan password tidak cocok, harap cek kembali.", 4000, "red")';
			}
		}$this->load->view('login', $data);
    }

	public function logout(){
		$this->session->sess_destroy();
		redirect('/');
	}
} 