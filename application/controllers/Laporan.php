<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('sudah_login')){
			redirect('login');
		}
	}

	public function index(){
		$data['user'] = $this->session->userdata('sudah_login');
		$data['konten'] = 'laporan/index';
		$this->load->view('layouts', $data);
	}

}
