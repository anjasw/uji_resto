<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('sudah_login')){
			redirect('login/index');
		}
	}

	public function index(){
		$data['user'] = $this->session->userdata('sudah_login');
		$data['konten'] = 'home';
		$this->load->view('layouts', $data);
	}

}
