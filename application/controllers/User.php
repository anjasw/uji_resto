<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('sudah_login')){
			redirect('login');
		}	
		$user = $this->session->userdata('sudah_login');
        if($user['level'] === "Pelanggan"){
			redirect('pesanan/tambah');            
        }
		$this->load->model('User_model','um');
	}

	public function index()
	{
		$data['konten'] = 'user/index';
		$data['user'] = $this->session->userdata('sudah_login');
		// var_dump($this->session->userdata());die();
		$user = $data['user'];
		$data['data'] = $this->um->data(false, $user['level'])->result();
		$this->load->view('layouts', $data);
	}
	public function tambah(){
		$data['konten'] = 'user/tambah';
		$data['user'] = $this->session->userdata('sudah_login');
		$data['level'] = $this->um->level()->result();
		if($this->input->method(TRUE) == "POST"){
			// var_dump($this->input->post());die();
			if($this->input->post('password') !== $this->input->post('password_konfirmasi')){
				echo "<script>alert('Password harus di konfirmasi.')</script>";
			}else{
				$tambah = array(
					"nama_user" => $this->input->post('nama_user'),
					"username" => $this->input->post('username'),
					"id_level" => $this->input->post('level'),
					"password" => md5($this->input->post('password_konfirmasi'))
				);
				if($this->um->tambah($tambah)){
					echo "<script>alert('Berhasil menambahkan data user.'); window.location.href='". base_url() ."user'</script>";				
				}
			}
			
		}
		$this->load->view('layouts', $data);
	}
	public function edit($id){
		$data['konten'] = 'user/edit';
		$data['user'] = $this->session->userdata('sudah_login');
		$data['data'] = $this->um->data($id)->row();
		// var_dump($data['data']->password);die();
		$data['level'] = $this->um->level()->result();
		if($this->input->method(TRUE) == "POST"){
			$user = $data['user'];
			$data_id = $data['data'];
			if($user['level'] !== "Administrator"){
				if(md5($this->input->post('password_lama')) !== $data_id->password){
					echo "<script>alert('Password lama harus sesuai dengan password sebelumnya.')</script>";
				}else{
					if($this->input->post('password') !== $this->input->post('password_konfirmasi')){
						echo "<script>alert('Password harus di konfirmasi.')</script>";
					}else{
						$edit = array(
							"nama_user" => $this->input->post('nama_user'),
							"username" => $this->input->post('username'),
							"id_level" => $this->input->post('level'),
							"password" => md5($this->input->post('password_konfirmasi'))
						);
						if($this->um->update($id, $edit)){
							echo "<script>alert('Berhasil mengedit data user.'); window.location.href='". base_url() ."user'</script>";				
						}
					}		
				}
			}else{
				if($this->input->post('password') !== $this->input->post('password_konfirmasi')){
					echo "<script>alert('Password harus di konfirmasi.')</script>";
				}else{
					$edit = array(
						"nama_user" => $this->input->post('nama_user'),
						"username" => $this->input->post('username'),
						"id_level" => $this->input->post('level'),
						"password" => md5($this->input->post('password_konfirmasi'))
					);
					if($this->um->update($id, $edit)){
						echo "<script>alert('Berhasil mengedit data user.'); window.location.href='". base_url() ."user'</script>";				
					}
				}
			}
			// var_dump($this->input->post());die();		
		}
		$this->load->view('layouts', $data);
	}

	public function hapus($id){
		if($this->um->hapus($id)){
            echo "<script>alert('Berhasil Menghapus Data User.'); window.location.href='".base_url()."user'</script>";
        }else{
            echo "<script>alert('Gagal Menghapus User.'); window.location.href='".base_url()."user'</script>";
        }
	}

}
