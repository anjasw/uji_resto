<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('sudah_login')){
			redirect('login');
		}
		$user = $this->session->userdata('sudah_login');
        if($user['level'] === "Pelanggan"){
			redirect('pesanan/tambah');            
		}
		$this->load->model('Transaksi_model', 'tm');
		$this->load->model('Order_model', 'om');
		$this->load->model('Meja_model', 'mm');
	}

	public function index()
	{
        $data['konten'] = 'transaksi/index';
		$data['user'] = $this->session->userdata('sudah_login');
		$data['pesan'] = $this->tm->data()->result();
		$user = $data['user'];
		$this->load->view('layouts', $data);
	}
	public function bayar($id){
		$pesan = $this->db->query("SELECT id_user FROM pesan WHERE id_order = '$id'")->row();
		$detail = $this->db->query("SELECT * FROM detail_order WHERE id_order = '$id'")->result();
		$harga = array();
		foreach($detail as $q){
			$masakan = $this->db->query("SELECT harga FROM masakan WHERE id_masakan = '$q->id_masakan'")->row();
			$harga[] = $masakan->harga*$q->jumlah;
		} 
		// var_dump(array_sum($harga));die();
		$total = array_sum($harga);
		$data = array(
			"id_user" => $pesan->id_user,
			"id_order" => $id,
			"tanggal" => date('Y-m-d H:i:s'),
			"total_bayar" => $total
		);
		if($this->tm->bayar($data)){
			$status = array(
				"status_order" => "Selesai"
			);
			if($this->om->update_status($id,$status)){
				$no = $this->mm->nomor($id);
				$meja = $this->mm->data($no->id_meja)->row();
				$no_meja = $meja->no_meja;
				$nstts = array(
					"status_meja" => "Tersedia"
				);
				if($this->mm->status($no_meja, $nstts)){
					redirect('transaksi');
				}
			}
		}
	}
	public function log(){
		$data['konten'] = 'transaksi/log';
		$data['user'] = $this->session->userdata('sudah_login');		
		$data['log'] = $this->tm->data_log()->result();
		$this->load->view('layouts', $data);
	}
}
