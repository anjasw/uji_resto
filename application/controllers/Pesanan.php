<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('sudah_login')){
			redirect('login');
		}
		$this->load->model('Order_model','om');
		$this->load->model('Masakan_model','mm');
		$this->load->model('Meja_model','meja');
	}

	public function index()
	{
		$user = $this->session->userdata('sudah_login');
        if($user['level'] === "Pelanggan"){
			redirect('pesanan/tambah');            
        }
		$data['konten'] = 'pesanan/index';
		$data['user'] = $this->session->userdata('sudah_login');
		$data['pesan'] = $this->om->data()->result();
		$this->load->view('layouts', $data);
	}
	public function tambah()
	{
		$data['user'] = $this->session->userdata('sudah_login');
		$data['konten'] = 'pesanan/tambah';
		$data['pesan'] = $this->mm->data(false, true)->result();
		$data['meja'] = $this->db->query("SELECT * FROM meja WHERE status_meja = 'Tersedia'")->result();
		if($this->input->method(TRUE) == "POST")
		{
			if($this->input->post('pesanan') == ""){
				echo "<script>alert('Masakan Belum Dipilih.'); window.location.href='".base_url()."pesanan/tambah'</script>";
			}else{
				$user = $data['user'];
				$meja = $this->input->post('no_meja');
				$id_masakan = $this->input->post('pesanan');
				$keterangan = $this->input->post('keterangan');
				$jumlah = $this->input->post('jumlah');
				// echo var_dump($totaldata);
				$insert = array(
					"id_meja" => $meja,
					"tanggal" => date('Y-m-d H:i:s'),
					"id_user" => $user['id_user'],
					"status_order" => "Proses"
				);
				$i = 0;
				if($this->om->insert($insert)){
					$datameja = array(
						"status_meja" => "Tidak Tersedia"
					);
					if($this->meja->status($meja, $datameja)){
						$id_order = $this->db->query("SELECT id_order FROM pesan ORDER BY id_order DESC LIMIT 1")->row();
						while($i < count($id_masakan)){
							// echo $totaldata[$i];
							$dade = array(
								"id_order" => $id_order->id_order,
								"id_masakan" => $id_masakan[$i],
								"keterangan" => $keterangan[$i],
								"jumlah" => $jumlah[$i]
							);		
							$this->om->detail($dade);				
							$i++;
						}
					}
				}
				redirect('pesanan');
			}
		}
		$this->load->view('layouts', $data);		
	}

	public function detail($id){
		// var_dump($this->om->detail_order($id)->result());
		$detail = $this->om->detail_order($id)->result();
		if(count($detail) > 0){
			// var_dump($detail);die();
		}else{
			show_404();die();
		}
		$data['konten'] = 'pesanan/detail';
		$data['detail'] = $detail;
		$data['user'] = $this->session->userdata('sudah_login');
		$this->load->view('layouts',$data);
	}

	public function batal($id){	
		$status = array(
			"status_order" => "Dibatalkan"
		);
		if($this->om->update_status($id,$status)){
			$no = $this->meja->nomor($id);
			$meja = $this->meja->data($no->id_meja)->row();
			$no_meja = $meja->no_meja;
			$nstts = array(
				"status_meja" => "Tersedia"
			);
			if($this->meja->status($no_meja, $nstts)){
				redirect('pesanan');
			}
		}
	}
}
