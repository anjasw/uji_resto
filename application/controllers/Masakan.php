<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masakan extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata('sudah_login')){
			redirect('login');
        }
        $user = $this->session->userdata('sudah_login');
        if($user['level'] === "Pelanggan"){
			redirect('pesanan/tambah');            
        }
        $this->load->model('Masakan_model','mm');
	}

	public function index()
	{
        $data['konten'] = 'masakan/index';
        $data['user'] = $this->session->userdata('sudah_login');
		$data['masakan'] = $this->mm->data(FALSE)->result();
		$this->load->view('layouts', $data);
    }
    public function edit($id){
        $data['masakan'] = $this->mm->data($id)->row();
        $data['konten'] = 'masakan/edit';
        $data['user'] = $this->session->userdata('sudah_login');
        if($this->input->method(TRUE) === "POST"){
            $config['upload_path']          = './gambar/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = date('Y-m-d H:i:s').$_FILES['gambar']['name'];
            $config['max_size']             = 7000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar')){
                $edit = array(
                    "nama_masakan" => $this->input->post('nama_masakan'),
                    "harga" => $this->input->post('harga'),
                    "status_masakan" => $this->input->post('status')
                );
                if($this->mm->update($id, $edit)){
                    redirect('masakan');
                }
            }else{
                $gambar = $this->upload->data();
                $insert = array(
                    "nama_masakan" => $this->input->post('nama_masakan'),
                    "harga" => $this->input->post('harga'),
                    "status_masakan" => $this->input->post('status'),
                    "gambar" => $gambar['file_name']
                );
                if($this->mm->update($id, $insert)){
                    redirect('masakan');
                }                
            }
        }
		$this->load->view('layouts', $data);
    }
    public function hapus($id){
        if($this->mm->hapus($id)){
            echo "<script>alert('Berhasil Menghapus Data.'); window.location.href='".base_url()."masakan'</script>";
        }else{
            echo "<script>alert('Gagal Menghapus Data.'); window.location.href='".base_url()."masakan'</script>";
        }
    }
    public function tambah(){
        $data['user'] = $this->session->userdata('sudah_login');
        $data['konten'] = 'masakan/tambah';
        if($this->input->method(TRUE) === "POST"){
        
            $config['upload_path']          = './gambar/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['file_name']            = date('Y-m-d H:i:s').$_FILES['gambar']['name'];
            $config['max_size']             = 7000;
        
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('gambar')){
                // var_dump($this->upload->display_errors());die();
                echo "<script>alert('Gagal mengupload gambar'); window.location.href='". base_url() ."masakan/tambah'</script>";
            }else{
                $gambar = $this->upload->data();
                $insert = array(
                    "nama_masakan" => $this->input->post('nama_masakan'),
                    "harga" => $this->input->post('harga'),
                    "status_masakan" => $this->input->post('status'),
                    "gambar" => $gambar['file_name']
                );
                if($this->mm->tambah($insert)){
                    redirect('masakan');
                }
            }
        }
        $this->load->view('layouts', $data);
    }

}
