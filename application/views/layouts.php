
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>TheBestResto</title>
    <!-- <link rel="icon" href="<?php //echo base_url() ?>assets/images/favicon/favicon-32x32.png" sizes="32x32"> -->
    <link href="<?php echo base_url() ?>assets/css//materialize.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css//style.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/custom/custom.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url() ?>assets/vendors/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/materialize.min.js"></script>
  </head>
  <body>
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-light-blue-cyan">
          <div class="nav-wrapper">
            <ul class="left">
              <li>
                <h1 class="logo-wrapper">
                  <a href="index.html" class="brand-logo darken-1">
                    <!-- <img src="<?php // echo base_url() ?>assets/images/logo/materialize-logo.png" alt="materialize logo"> -->
                    <span class="logo-text hide-on-med-and-down">TheBestResto</span>
                  </a>
                </h1>
              </li>
            </ul>
            <div class="header-search-wrapper hide-on-med-and-down">
              <i class="material-icons">search</i>
              <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore TheBestResto" />
            </div>
            <ul class="right hide-on-med-and-down">
              <li style="padding-right: 60px;">
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- end header nav-->
    </header>
    <div id="main">
      <div class="wrapper">
        <aside id="left-sidebar-nav">
          <ul id="slide-out" class="side-nav fixed leftside-navigation">
            <li class="user-details cyan darken-2">
              <div class="row">
                <div class="col col s4 m4 l4">
                  <img src="<?php echo base_url() ?>assets/images/avatar/avatar-7.png" alt="" class="circle responsive-img valign profile-image cyan">
                </div>
                <div class="col col s8 m8 l8">
                  <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown-nav"><?= $user['nama'] ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                  <p class="user-roal"><?= $user['level'] ?></p>
                </div>
              </div>
            </li>
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a href="<?php echo base_url() ?>" class="waves-effect waves-cyan">
                      <i class="material-icons">pie_chart_outlined</i>
                      <span class="nav-text">Dashboard</span>
                    </a>
                </li>
                <?php if($user['level'] === "Administrator"){ ?>
                  <li class="bold">
                  <a href="<?php echo base_url() ?>masakan" class="waves-effect waves-cyan">
                      <i class="material-icons">local_dining</i>
                      <span class="nav-text">Masakan</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($user['level'] === "Administrator" || $user['level'] === "Waiter"){ ?>
                <li class="bold">
                  <a href="<?php echo base_url() ?>pesanan" class="waves-effect waves-cyan">
                      <i class="material-icons">reorder</i>
                      <span class="nav-text">Pesanan</span>
                    </a>
                </li>
                <?php } ?>
                
                <?php if($user['level'] === "Administrator" || $user['level'] === "Kasir"){ ?>                
                <li class="bold">
                  <a href="<?php echo base_url() ?>transaksi" class="waves-effect waves-cyan">
                      <i class="material-icons">shopping_cart</i>
                      <span class="nav-text">Transaksi</span>
                    </a>
                </li>
                <li class="bold">
                  <a href="<?php echo base_url() ?>transaksi/log" class="waves-effect waves-cyan">
                      <i class="material-icons">shopping_cart</i>
                      <span class="nav-text">Catatan Transaksi</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($user['level'] === "Administrator" || $user['level'] === "Kasir" || $user['level'] === "Waiter"){ ?>                                
                <li class="bold">
                  <a href="<?php echo base_url() ?>user" class="waves-effect waves-cyan">
                      <i class="material-icons">account_box</i>
                      <span class="nav-text">Kelola User</span>
                    </a>
                </li>
                <?php } ?>     
                <?php if($user['level'] !== "Pelanggan"){ ?>           
                <li class="bold">
                  <a href="<?php echo base_url() ?>laporan" class="waves-effect waves-cyan">
                      <i class="material-icons">local_printshop</i>
                      <span class="nav-text">Generate Laporan</span>
                    </a>
                </li>
                <?php } ?>
                <li class="bold">
                  <a href="<?= base_url()?>login/logout" class="waves-effect waves-cyan">
                    <i class="material-icons">lock</i>
                    <span class="nav-text">Logout</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <section id="content">
          <div class="container">
            <?php $this->load->view($konten); ?>
          </div>
        </section>
      </div>
    </div>
    <footer class="page-footer gradient-45deg-light-blue-cyan">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright © oleh
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-lighten-2" href="http://anjas-blog.firebaseapp.com/" target="_blank">Anjas Wicaksana</a></span>
            <span class="right hide-on-small-only"> Didesain dan Dikembangkan oleh <a class="grey-text text-lighten-2" href="https://anjas-blog.firebaseapp.com/">Anjas Wicaksana</a></span>
          </div>
        </div>
    </footer>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/custom-script.js"></script>
  </body>
</html>