<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Form Tambah User</h4>
                <div class="row">
                    <form class="col s12 m12 l12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col s6">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="nama_user" type="text" required value="<?= $data->nama_user ?>">
                                        <label for="nama_user">Nama Lengkap</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="level" id="">
                                            <option value="<?= $data->id_level ?>"><?= $data->nama_level ?></option>                                    
                                            <?php foreach($level as $q): ?>
                                            <option value="<?= $q->id_level ?>"><?= $q->nama_level ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <label for="level">Level User</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="username" type="text" required value="<?= $data->username ?>">
                                        <label for="username">Username</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6">
                                <?php 
                                if($user['level'] !== "Administrator"){ ?>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="password_lama" type="password" required value="<?= $this->input->post('password_lama') ?>">
                                        <label for="password_lama">Password Lama</label>
                                    </div>
                                </div>
                                <?php } ?>
                                
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="password" type="password" required value="<?= $this->input->post('password') ?>">
                                        <label for="password">Password Baru</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="password_konfirmasi" type="password" required value="<?= $this->input->post('password_konfirmasi') ?>">
                                        <label for="password_konfirmasi">Konfirmasi Password</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#preview').hide();

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
                $('#preview').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function() {
        readURL(this);
    });
</script>