<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Form Tambah User</h4>
                <div class="row">
                    <form class="col s12 m12 l12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col s6">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="nama_user" type="text" required value="<?= $this->input->post('nama_user') ?>">
                                        <label for="nama_user">Nama Lengkap</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select name="level" id="">
                                            <?php
                                            $pelanggan = $this->db->query("SELECT * FROM level WHERE nama_level = 'Pelanggan'")->row(); 
                                            $kasir = $this->db->query("SELECT * FROM level WHERE nama_level = 'Kasir'")->row(); 
                                            $waiter = $this->db->query("SELECT * FROM level WHERE nama_level = 'Waiter'")->row(); 
                                            if($user['level'] === "Waiter"){ ?>
                                                <option value="<?= $pelanggan->id_level ?>"><?= $pelanggan->nama_level ?></option>
                                                <option value="<?= $waiter->id_level ?>"><?= $waiter->nama_level ?></option>
                                            <?php }elseif($user['level'] === "Kasir"){ ?>
                                                <option value="<?= $kasir->id_level ?>"><?= $kasir->nama_level ?></option>                                            
                                            <?php }else{ ?>
                                                <?php foreach($level as $data): ?>
                                                <option value="<?= $data->id_level ?>"><?= $data->nama_level ?></option>
                                                <?php endforeach; ?>
                                            <?php } ?>
                                            
                                        </select>
                                        <label for="level">Level User</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col s6">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="username" type="text" required value="<?= $this->input->post('username') ?>">
                                        <label for="username">Username</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="password" type="password" required value="<?= $this->input->post('password') ?>">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="password_konfirmasi" type="password" required value="<?= $this->input->post('password_konfirmasi') ?>">
                                        <label for="password_konfirmasi">Konfirmasi Password</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#preview').hide();

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
                $('#preview').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function() {
        readURL(this);
    });
</script>