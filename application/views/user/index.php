<div id="horizontal-card" class="section">
    <!-- <h4 class="header">Horizontal Card</h4> -->
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h4 style="margin-left: 20px">Data Seluruh User</h4>
                            <div class="card-content">
                                <div id="responsive-table">
                                    <!-- <h4 class="header">Responsive Table</h4> -->
                                    <div class="row">
                                        <div class="col s12">
                                            <a href="<?php echo base_url() ?>user/tambah" class="btn blue">Tambah Data</a>
                                        </div>
                                        <br><br>
                                        <div class="col s12">
                                        <table class="responsive-table centered bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama User</th>
                                                    <th>Username </th>
                                                    <th>Level User</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; foreach($data as $a): ?>
                                                    <tr>
                                                        <td><?= $no++ ?></td>
                                                        <td><?php echo ucwords($a->nama_user) ?></td>
                                                        <td><?= $a->username ?></td>
                                                        <td><?= $a->nama_level ?></td>
                                                        <td><a href="<?php echo base_url().'user/edit/'. $a->id_user ?>" class="btn blue"><i class="material-icons">edit</i></a>  <a href="<?php echo base_url().'user/hapus/'. $a->id_user ?>" class="btn red"><i class="material-icons">delete</i></a></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>