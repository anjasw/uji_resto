<div id="horizontal-card" class="section">
    <!-- <h4 class="header">Horizontal Card</h4> -->
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h4 style="margin-left: 20px">Data Seluruh Masakan</h4>
                            <div class="card-content">
                                <div id="responsive-table">
                                    <!-- <h4 class="header">Responsive Table</h4> -->
                                    <div class="row">
                                        <div class="col s12">
                                            <a href="<?php echo base_url() ?>masakan/tambah" class="btn blue">Tambah Data</a>
                                        </div>
                                        <br><br>
                                        <div class="col s12">
                                        <table class="responsive-table centered bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama Masakan</th>
                                                    <th>Gambar</th>
                                                    <th>Harga</th>
                                                    <th>Status masakan</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; foreach($masakan as $data): ?>
                                                    <tr>
                                                        <td><?= $no++ ?></td>
                                                        <td><?php echo ucwords($data->nama_masakan) ?></td>
                                                        <td><img class="img-responsive" width="100" src="<?php echo base_url(). 'gambar/'. $data->gambar ?>" /></td>
                                                        <td>Rp. <?= $data->harga ?></td>
                                                        <td><?= $data->status_masakan ?></td>
                                                        <td><a href="<?php echo base_url().'masakan/edit/'. $data->id_masakan ?>" class="btn blue"><i class="material-icons">edit</i></a>  <a href="<?php echo base_url().'masakan/hapus/'. $data->id_masakan ?>" class="btn red"><i class="material-icons">delete</i></a></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>