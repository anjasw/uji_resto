<div id="basic-form" class="section">
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card-panel">
                <h4 class="header2">Form Tambah List Masakan</h4>
                <div class="row">
                    <form class="col s12 m12 l12" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col s6">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="nama_masakan" type="text" required value="<?php echo $masakan->nama_masakan ?>">
                                        <label for="nama_masakan">Nama Masakan</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input name="harga" type="number" min="0" required value="<?php echo $masakan->harga ?>">
                                        <label for="harga">Harga</label>
                                    </div>
                                </div>
                                <div class="row">
                                <?php  
                                    if($masakan->status_masakan == "Tersedia"){ ?>
                                    <p>
                                        <input class="with-gap" name="status" value="Tersedia" type="radio" id="test2" checked/>
                                        <label for="test2">Tersedia</label>
                                    </p>
                                    <p>
                                        <input class="with-gap" name="status" value="Tidak Tersedia" type="radio" id="test3"/>
                                        <label for="test3">Tidak Tersedia</label>
                                    </p>
                                    <?php }else{ ?>
                                    <p>
                                        <input class="with-gap" name="status" value="Tersedia" type="radio" id="test2"/>
                                        <label for="test2">Tersedia</label>
                                    </p>
                                    <p>
                                        <input class="with-gap" name="status" value="Tidak Tersedia" type="radio" id="test3" checked />
                                        <label for="test3">Tidak Tersedia</label>
                                    </p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col s6">
                                <div class="row">
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>File</span>
                                            <input type="file" id="gambar" name="gambar">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" name="nama_gambar" value="<?php echo $masakan->gambar ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <img id="preview" src="<?php echo base_url().'gambar/'.$masakan->gambar ?>" alt="your image" width="100%"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // $('#preview').hide();

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
                // $('#preview').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#gambar").change(function() {
        readURL(this);
    });
</script>