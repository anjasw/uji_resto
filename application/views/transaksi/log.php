<div id="horizontal-card" class="section">
    <!-- <h4 class="header">Horizontal Card</h4> -->
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h4 style="margin-left: 20px">Catata transaksi</h4>
                            <div class="card-content">
                                <div id="responsive-table">
                                    <!-- <h4 class="header">Responsive Table</h4> -->
                                    <div class="row">
                                        <div class="col s12">
                                        <table class="responsive-table centered bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama User</th>
                                                    <th>Level User</th>
                                                    <th>Nomor Meja</th>
                                                    <th>Tanggal</th>
                                                    <th>Total Bayar</th>
                                                    <th>Status Order</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; foreach($log as $data): ?>
                                                    <tr>
                                                        <td><?= $no ?></td>
                                                        <td><?php echo $data->nama_user ?></td>
                                                        <td><?php echo $data->nama_level ?></td>
                                                        <td><?php echo $data->no_meja ?></td>
                                                        <td><?php echo $data->tanggal ?></td>
                                                        <td><?php echo $data->total_bayar ?></td>
                                                        <td><?php echo $data->status_order ?></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>