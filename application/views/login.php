<html>
    <head>
        <title>Halaman Login</title>
            <!-- <link rel="icon" href="<?php //echo base_url() ?>assets/images/favicon/favicon-32x32.png" sizes="32x32"> -->
        <link href="<?php echo base_url() ?>assets/css//materialize.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url() ?>assets/vendors/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/materialize.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h2 class="center">Harap Login</h2>
                    
                </div>
            </div>
            <div class="row">
                <div class="col s3"></div>
                <div class="col s6">
                <!-- <p class="red"></p> -->
                <script><?= $msg ?></script>
                    <form action="" method="POST">
                        <div class="input-field">
                            <input type="text" name="username" value="<?= $this->input->post('username') ?>" required>
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" value="<?= $this->input->post('password') ?>" required>
                            <label for="password">Password</label>
                        </div>
                        <button type="submit" class="btn pink right">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>