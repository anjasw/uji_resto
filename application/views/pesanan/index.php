<div id="horizontal-card" class="section">
    <!-- <h4 class="header">Horizontal Card</h4> -->
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h4 style="margin-left: 20px">Data Pesanan</h4>
                            <div class="card-content">
                                <div id="responsive-table">
                                    <!-- <h4 class="header">Responsive Table</h4> -->
                                    <div class="row">
                                        <div class="col s12">
                                            <a href="<?php echo base_url() ?>pesanan/tambah" class="btn blue">Tambah Pesanan</a>
                                        </div>
                                        <br><br>
                                        <div class="col s12">
                                        <table class="responsive-table centered bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nama User</th>
                                                    <th>Level User</th>
                                                    <th>Nomor Meja</th>
                                                    <th>Tanggal</th>
                                                    <th>Status Order</th>
                                                    <th>Total Harga</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                
                                                    <?php $no = 1; foreach($pesan as $data): ?>
                                                    <tr>
                                                    <?php  
                                                        $detail = $this->db->query("SELECT * FROM detail_order WHERE id_order = '$data->id_order'")->result();
                                                        $harga = array();
                                                        foreach($detail as $q){
                                                            $masakan = $this->db->query("SELECT harga FROM masakan WHERE id_masakan = '$q->id_masakan'")->row();
                                                            $harga[] = $masakan->harga*$q->jumlah;
                                                        } 
                                                        // var_dump($harga);die();
                                                        $total = array_sum($harga);
                                                    ?>
                                                        <td><?= $no ?></td>
                                                        <td><?php echo $data->nama_user ?></td>
                                                        <td><?php echo $data->nama_level ?></td>
                                                        <td><?php echo $data->no_meja ?></td>
                                                        <td><?php echo $data->tanggal ?></td>
                                                        <td><?php echo $data->status_order ?></td>
                                                        <td><?php echo $total ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url().'pesanan/detail/'. $data->id_order ?>" class="btn blue"><i class="material-icons">reorder</i></a>
                                                            <?php
                                                                if($data->status_order !== "Selesai"){
                                                            ?>
                                                            <a href="<?php echo base_url().'pesanan/batal/'. $data->id_order ?>" class="btn red"><i class="material-icons">cancel</i></a>
                                                            <?php    }
                                                            ?>  
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>