<div class="divider"></div>
<form method="POST" id="form">
    <div id="image-card" class="section">
        <h4 class="header">List Masakan Yang Tersedia</h4>
        <div class="row">
            <div class="input-field col l10 m10 s8">
                <select name="no_meja">
                    <option value="" disabled selected> Pilih nomor meja </option>
                    <?php foreach($meja as $q): ?>
                        <option value="<?= $q->id_meja ?>"><?= $q->no_meja ?></option>
                    <?php endforeach; ?>
                </select>
                <label for="no_meja">Nomor Meja</label>
            </div>
            <div class="input-field col l2 m2 s1">
                <button type="submit" class="btn red">Pesan</button> 
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l12">
                <?php $id = 1; foreach($pesan as $data): ?>
                <div class="row">
                    <div class="col s12 m8 l4">
                        <div class="card">
                            <div class="card-image">
                                <img src="<?php echo base_url().'gambar/'.$data->gambar ?>" alt="sample">
                            </div>
                            <div class="card-content">
                                <p><?= ucwords(strtolower($data->nama_masakan)) ?></p><br>
                                <p style="color:orange">Rp. <?php echo number_format($data->harga,0,"",".") ?></p>

                            </div>
                            <div class="card-action">
                                <!-- <a href="#!" class="waves-effect waves-light btn gradient-45deg-red-pink">Button</a> -->
                                <p>
                                    <input type="checkbox" name="pesanan[]" id="<?php echo $id ?>" value="<?php echo $data->id_masakan ?>">
                                    <label for="<?php echo $id ?>">Tambah Ke List Pesanan.</label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l8" id="pesan<?= $id ?>" class"keterangan">
                        <div class="input-field col s12">
                            <textarea id="messages<?php echo $id ?>" class="materialize-textarea" name="keterangan[]"></textarea>
                            <label for="messages<?php echo $id ?>">Catatan Untuk Masakannya</label>
                        </div>
                        <div class="input-field col s12">
                            <input type="number" name="jumlah[]">
                            <label for="jumlah">Jumlahnya ..</label>
                        </div>
                    </div>
                </div>
                <?php $id++; endforeach; ?>
            </div>
        </div>
    </div>
</form>
<script>
// $('.keterangan').hide();
<?php $id = 1; foreach($pesan as $data): ?>
$('#pesan<?php echo $id ?>').hide()

<?php $id++; endforeach; ?>
 $('#form input:checkbox').each(function() {
      checkbox_check(this);
      console.log(this)
    });

    // Task check box
    $('#form input:checkbox').change(function() {
      checkbox_check(this);
      console.log(this);
    });

function checkbox_check(el) {
    if (!$(el).is(':checked')) {  
    //   $(el).next().css('text-decoration', 'none'); // or addClass
        $('#pesan'+$(el).prop('id')).hide()
    
    } else {
        $('#pesan'+$(el).prop('id')).show()
        
    //   $(el).next().css('text-decoration', 'line-through'); //or addClass
    }
}
</script>