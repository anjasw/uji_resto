<div id="horizontal-card" class="section">
    <!-- <h4 class="header">Horizontal Card</h4> -->
    <div class="row">
        <div class="col s12 m12 l12">
            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <h4 style="margin-left: 20px">Data Pesanan</h4>
                            <div class="card-content">
                                <div id="responsive-table">
                                    <!-- <h4 class="header">Responsive Table</h4> -->
                                    <div class="row">
                                        <div class="col s12">
                                        <table class="responsive-table centered bordered">
                                            <thead>
                                                <tr>
                                                    <th>Nama Masakan</th>
                                                    <th>Jumlah</th>
                                                    <th>Keterangan</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; foreach($detail as $data): ?>
                                                    <tr>
                                                        <td><?php echo $data->nama_masakan ?></td>
                                                        <td><?php echo $data->jumlah ?></td>
                                                        <td><?php echo $data->keterangan ?></td>
                                                        <td><a href="<?php echo base_url().'pesanan/detail/'. $data->id_order ?>" class="btn blue"><i class="material-icons">reorder</i></a>  <a href="<?php echo base_url().'pesanan/hapus/'. $data->id_order ?>" class="btn red"><i class="material-icons">delete</i></a></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>